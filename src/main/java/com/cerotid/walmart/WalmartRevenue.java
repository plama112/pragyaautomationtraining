package com.cerotid.walmart;

import java.util.Date;

public class WalmartRevenue {
	private double revenue;

	public double getRevenue() {
		return revenue;
	}

	public void oilChange(Cars car) {
		car.setLastoilChangeDate(new Date());
		if (car instanceof Coupe) {
			revenue += 49.99;
		} else if (car instanceof Sedan) {
			revenue += 39.99;
		} else if (car instanceof Hatchback) {
			revenue += 29.99;
		}
	}
	public void tireChange(Cars car) {
		//car.setLastoilChangeDate(new Date());
		car.setLastTireChangeDate(new Date());
	}
}