package com.cerotid.walmart;

import java.util.List;

public class CarFactory {
	
	public static Cars getCar(String CarType) throws Exception { 
		Cars newCar = null;
	      if(CarType.equalsIgnoreCase("Camero")) {
	         newCar = new Camero();
	      } else if(CarType.equalsIgnoreCase("MUSTANG")){
	          newCar = new Mustang();
	      } else if(CarType.equalsIgnoreCase("CHALLENGER")){
	         newCar = new Challenger();
	      } else if(CarType.equalsIgnoreCase("VELOSTER")){
		       newCar = new Veloster();
		  } else if(CarType.equalsIgnoreCase("SPARK")){
		        newCar = new Spark();
		  } else if(CarType.equalsIgnoreCase("COUPE")){
			      newCar = new Coupe();
		  } else if(CarType.equalsIgnoreCase("HATCHBACK")){
		          newCar = new Hatchback();
		  } else if(CarType.equalsIgnoreCase("SEDAN")){
		        newCar = new Sedan();
		  } else  {
			  throw new Exception("Car Type not available");
			  
		  }
	      return newCar;
	}
	public static Cars getCarByVinNumber(String vin, List<Cars> cars) throws Exception {
		for (Cars c : cars) {
			if(c.getVin().equals(vin)) {
				 //System.out.println("Car has AndroidAuto: " + c.isHasAndroidAuto());
				//System.out.println("Car has CarPlay:" + c.isHasCarPlay());
				return c;
				
			}
			
		}
		throw new Exception("There is no Car");
	}
}

//public static Cars getRegularService(String vinNo, List<Cars> cars ) throws Exception  {
//	for(Cars c : cars) {
//		if(c.getVin().equals(vinNo)) {
//						return c;
//		}
//	}
//	
//          throw new Exception("There is no Car");
//}
//}
//
