package com.cerotid.walmart;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.cerotid.walmart.Cars;
import com.cerotid.walmart.CarFactory;
import com.cerotid.walmart.WalmartRevenue;



public class RichMan {
	
	List<Cars> myCars = new ArrayList<>();

	public void setMyCars(List<Cars> myCars) {
	 this.myCars = myCars;
	
	}

	public List<Cars> getMyCars() {
		return myCars;
	}

	Scanner in = new Scanner(System.in);
	WalmartRevenue wr = new WalmartRevenue();

	public void displayMenu() {
		String choice;
		
		boolean isExit = false;
		while (isExit == false) {

			System.out.println("Choose one of the option");
			System.out.println("Press A to Add a New Car");
			System.out.println("Press B Modify Cars Features");
			System.out.println("Press C Repaint");
			System.out.println("Press D to Perform Regular Services");
			System.out.println("Press E Exit");
			choice = in.nextLine().toUpperCase();
			switch (choice) {
			case "A":
				System.out.println("Choose from the following options: "
						+ "Camero, Challenger, Mustang, Coupe, Sedan, Hatchback, Veloster, Spark");
				try {
					String CarType = in.nextLine();
					Cars newCar = CarFactory.getCar(CarType);
					System.out.println("Enter Vin Number for new car");
					newCar.setVin(in.nextLine());
					//System.out.println(newCar.getVin());
					System.out.println("New Car Added: " + CarType);
					this.addCar(newCar);
				} catch (Exception e) {
					e.getMessage();
					e.printStackTrace();
					
				}
				break;
			
		
				
			case "B":
				System.out.println("Enter vin number to modify the carFeatures");
				
				try {
					
					this.modify(CarFactory.getCarByVinNumber(in.nextLine(), myCars));
					System.out.println("Vin has been Modified");
				
					
				} catch (Exception e) {
					System.out.println("Error occured while modifying car" + e.getMessage());
				}
				break;
		
		
			case "C":
				System.out.println("Enter vin to modify");
				String vin = in.nextLine();
				Cars Repaint;
				try {
					Repaint = CarFactory.getCarByVinNumber(vin, myCars);
					System.out.println("Enter new color");
					String newColor = in.nextLine();
					System.out.println("Enter for new color change from " + newColor + " now." );
					String modifyColor = in.nextLine();
					System.out.println("You have changed the color " + modifyColor);
					this.repaint(Repaint, newColor);
					this.repaint(Repaint, modifyColor);
				} catch (Exception e) {
					System.out.println("Error occured while recoloring the car");
				}
				break;
			case "D":
				System.out.println("Enter vin to modify");
				String vinNo = in.nextLine();
				try {
					Cars carToService = CarFactory.getCarByVinNumber(vinNo, myCars);
					System.out.println("Enter 1 for oil change or 2 for tire change");
					int oilOrTireChange = in.nextInt();
					this.performService(carToService, oilOrTireChange);
				} catch (Exception e) {
					System.out.println("Error occured while servicing your car");
				}
				break;
			case "E":
				isExit = true;
				break;
		}
		}
		}

	
	
	public void addCar(Cars c) {
		this.myCars.add(c);
	}

	public void modify(Cars c) {
		System.out.println("Car has AndroidAuto : " + c.isHasAndroidAuto());
		System.out.println("Car has CarPlay : " + c.isHasCarPlay());
	}

	public void repaint(Cars c, String newColor) {
		c.setColors(newColor);
	}					
	
	public void performService(Cars c, int oilOrTireChange) {
		if (oilOrTireChange == 1) {
			wr.oilChange(c);
			System.out.println("User has Selected " + oilOrTireChange + " for " + c.getLastoilChangeDate());
		} else {
			wr.tireChange(c);
			System.out.println("User has Selected " + oilOrTireChange + " for " + c.getLastTireChangeDate());
		}
	}

	public static void main(String[] args) {
		RichMan man = new RichMan();
		man.displayMenu();
	}
	
	
}
