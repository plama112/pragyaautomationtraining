package com.cerotid.walmart;

import java.util.Date;

public abstract class Cars {
	private Engine engine;
	private Tires tires;
	private int doors;
	private int maxSpeed;
	private boolean hasAndroidAuto;
	private boolean hasCarPlay;
	private String Vin;
	private Date LastoilChangeDate;
	private Date LastTireChangeDate;
	private String colors;
	
	
	
	//Default Constructor
	public Cars() {
		
	}
 	
	//Overloaded Constructor
	
	public Cars(Engine engine, Tires tires, int doors, int maxSpeed, boolean hasAndroidAuto, boolean hasCarPlay,
			String vin, Date lastoilChangeDate, Date lastTireChangeDate, String colors) {
		this.engine = engine;
		this.tires = tires;
		this.doors = doors;
		this.maxSpeed = maxSpeed;
		this.hasAndroidAuto = hasAndroidAuto;
		this.hasCarPlay = hasCarPlay;
		Vin = vin;
		LastoilChangeDate = lastoilChangeDate;
		LastTireChangeDate = lastTireChangeDate;
		this.colors = colors;
	}
	
	
	
	
	

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public Tires getTires() {
		return tires;
	}

	public void setTires(Tires tires) {
		this.tires = tires;
	}

	public int getDoors() {
		return doors;
	}

	public void setDoors(int doors) {
		this.doors = doors;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public boolean isHasAndroidAuto() {
		
		return true;
	}

	public void setHasAndroidAuto(boolean hasAndroidAuto) {
		this.hasAndroidAuto = hasAndroidAuto;
	}

	public boolean isHasCarPlay() {
		
		return true;
	}

	public void setHasCarPlay(boolean hasCarPlay) {
		this.hasCarPlay = hasCarPlay;
	}

	public String getVin() {
		return Vin;
	}

	public void setVin(String vin) {
		Vin = vin;
	}

	public Date getLastoilChangeDate() {
		return LastoilChangeDate;
	}

	public void setLastoilChangeDate(Date lastoilChangeDate) {
		this.LastoilChangeDate = lastoilChangeDate;
	}

	public Date getLastTireChangeDate() {
		return LastTireChangeDate;
	}

	public void setLastTireChangeDate(Date lastTireChangeDate) {
		this.LastTireChangeDate = lastTireChangeDate;
	}

	public String getColors() {
		return colors;
	}

	public void setColors(String colors) {
		this.colors = colors;
	}
	
	

	@Override
	public String toString() {
		return "Cars [engine=" + engine + ", tires=" + tires + ", doors=" + doors + ", maxSpeed=" + maxSpeed
				+ ", hasAndroidAuto=" + hasAndroidAuto + ", hasCarPlay=" + hasCarPlay + ", Vin=" + Vin
				+ ", LastoilChangeDate=" + LastoilChangeDate + ", LastTireChange=" + LastTireChangeDate + ", colors="
				+ colors + "]";
	}

}
		