package com.cerotid.walmart;

public class Tires {
	public String modelNumber;
	public String manufacturer;
	
     //Default Constructor
	public Tires() {
		
	}
     
	//OverloadConstructor
	public Tires(String modelNumber, String manufacturer) {
		super();
		this.modelNumber = modelNumber;
		this.manufacturer = manufacturer;
	}

	//generating getter and setter method
	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	//generating to strings
	@Override
	public String toString() {
		return "Tires [modelNumber=" + modelNumber + ", manufacturer=" + manufacturer + "]";
	}
	

}

