package com.cerotid.walmart;

public class Engine {
	private String model;
	private String type;
	private String oilType;
	
    //default constructor
	public Engine() {
	
	}
     
	//overload constructor
	public Engine(String model, String type, String oilType) {
		super();
		this.model = model;
		this.type = type;
		this.oilType = oilType;
	}

	  //creating getter and setter
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOilType() {
		return oilType;
	}

	public void setOilType(String oilType) {
		this.oilType = oilType;
	}
 
	//Generating to String
	@Override
	public String toString() {
		return "Engine [model=" + model + ", type=" + type + ", oilType=" + oilType + "]";
	}
	
}
