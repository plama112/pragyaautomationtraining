package com.cerotid.qahomework;

public class Arrays {
	
	public static void main(String[] args) {
		int evenCount = 0;
		int sum = 0;
		int[] a = { 1, 7, 9, 20, 8 };
		int high = a[0];
		for (int i = 0; i < a.length; i++) {
			if (high < a[i]) {
				high = a[i];
			}
		}
		System.out.println("Highest Number is " + high);
		{
			for (int i = 0; i < a.length; i++) {
				if (a[i] % 2 == 0) {
					evenCount++;
				}
			}
			System.out.println("All Even Numbers is " + evenCount);
			{
				for (int i = 0; i < a.length; i++)
					sum = sum + a[i];
			}
		}
		System.out.println("Sum of numbers is " + sum);
		System.out.println("Mean Value is " + sum / a.length);

	}

}
