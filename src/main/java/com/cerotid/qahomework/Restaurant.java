package com.cerotid.qahomework;

import java.util.Scanner;

public class Restaurant {

	public static void main(String[] args) {

	    String choice;

		String order;
		
		Scanner userInput = new Scanner(System.in);
		boolean isExit = false;
		while (isExit == false) {
		
		System.out.println( "Press A for Appetizer ");
		System.out.println( "Press M for Main Course ");
		System.out.println( "Press S for Soup and Salads ");
		System.out.println( "Press D for Drinks");
		
		System.out.println("Enter your order ");
		
		
		
		choice = userInput.next().toUpperCase();

		switch (choice) {
		case "A":
			order = "Spring Rolls";
			break;

		case "M":
			order = "Thali";
			break;

		case "S":
			order = "Chicken Soup";
			break;

		case "D":
			order = "Coke";
			isExit = true;
			break;
		default:
			order = "not in menu";
		}
		
		
		System.out.println("Your Order is " + order);
	}

}
}
