package com.cerotid.qajunit;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cerotid.walmart.Camero;
import com.cerotid.walmart.Cars;
import com.cerotid.walmart.Challenger;
import com.cerotid.walmart.Mustang;
import com.cerotid.walmart.Sedan;
import com.cerotid.walmart.Spark;
import com.cerotid.walmart.Veloster;
import com.cerotid.walmart.WalmartRevenue;

public class WalmartRevenueTest {
	private List<Cars> cars = new ArrayList<Cars>();
	private WalmartRevenue revenue = new WalmartRevenue();

	@Before
	public void setUp() {
		for (int i = 0; i < 20; i++) {
			if (i < 10) {
				cars.add(new Sedan());
			} else if (i < 12) {
				cars.add(new Spark());
			} else if (i < 16) {
				cars.add(new Veloster());
			} else if (i < 18) {
				cars.add(new Mustang());
			} else if (i < 19) {
				cars.add(new Camero());
			} else {
				cars.add(new Challenger());
			}
		}
	}

	@Test
	public void testRevenue() {
		for (Cars car : cars) {
			revenue.oilChange(car);
		}
		Assert.assertEquals(779.7999, revenue.getRevenue(), 0.1);

	}

}
