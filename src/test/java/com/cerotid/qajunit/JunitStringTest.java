package com.cerotid.qajunit;

import org.junit.Assert;
import org.junit.Test;
import com.cerotid.qahomework.JunitsTwoString;

public class JunitStringTest {

	@Test
	public void onestringissubstringofotherString() {
	JunitsTwoString JunitsTwoString = new JunitsTwoString();
	Assert.assertTrue("geeksforgeeks".contains("for"));
	Assert.assertFalse("geeksforgeeks".contains("gap"));
	}

}
